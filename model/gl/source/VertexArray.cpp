//
// Created by pawel on 11.07.2021.
//

#include "../header/VertexArray.h"


VertexArray::VertexArray() {
    glGenVertexArrays(1, &renderer);
}

void VertexArray::addBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout) {
    bind();
    vb.bind();
    const auto &elements = layout.getElements();
    size_t offset = 0;
    for (unsigned int i = 0; i < elements.size(); ++i) {
        const auto &element = elements.at(i);
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i, element.count, element.type, element.normalized ? GL_TRUE : GL_FALSE,
                              layout.getStride(), (const void *) offset);
        offset += element.count * VertexBufferElements::getSizeofType(element.type);
    }
}

void VertexArray::bind() const {
    glBindVertexArray(renderer);
};

void VertexArray::unbind() {
    glBindVertexArray(0);
}

VertexArray::~VertexArray() {

};