//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_DIRT_H
#define VOXEL_ENGINE_DIRT_H

#include "../BlockType.h"

class Dirt : public BlockType {
public:
    Dirt();
};


#endif //VOXEL_ENGINE_DIRT_H
