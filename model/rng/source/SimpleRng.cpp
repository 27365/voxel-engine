//
// Created by Michał Sobański on 12.09.2021.
//

#include "../header/SimpleRng.h"


SimpleRng::SimpleRng(uint64_t seed) {
    this->seed = seed;
    rand();
}

void SimpleRng::rand() {
    seed = (seed * MULTIPLIER + INCREMENT) % MODULUS;
}

/* double value between 0.0 and 1.0 */
double SimpleRng::randDouble() {
    rand();
    return ((double) seed) / (MODULUS - 1.0);
}

double SimpleRng::randDouble(double min, double max) {
    return min + ((max - min) * randDouble());
}

uint64_t SimpleRng::randInt(uint64_t min, uint64_t max) {
    rand();
    return min + (seed % (max - min + 1));
}

bool SimpleRng::randBool() {
    rand();
    return (bool) (seed % 2);
}

/*
 * probability - double value between 0.0 and 1.0
 */
bool SimpleRng::withProb(double probability) {
    return randDouble() <= probability;
}
