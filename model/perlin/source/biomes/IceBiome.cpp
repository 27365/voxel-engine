//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/IceBiome.h"

IceBiome::IceBiome() : Biome(Biome::ICE, NOT_APPLICABLE, NOT_APPLICABLE, Biome::NO_PLANTS, {
        BlockType::SAND,
        BlockType::SAND,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}

void IceBiome::calcFaces(BiomeFragment *frag) {
    uint32_t z = BEACH_MIN_Z;
    Faces faces;
    faces.type = BlockType::getBlockType(BlockType::ICE);
    faces.up = true;
    faces.down = true;
    faces.north = (*frag->nB == waterBiome);
    faces.west = (*frag->wB == waterBiome);
    faces.south = (*frag->sB == waterBiome);
    faces.east = (*frag->eB == waterBiome);
    addBlock(frag, &z, &faces);
    Biome::calcFaces(frag);
}
