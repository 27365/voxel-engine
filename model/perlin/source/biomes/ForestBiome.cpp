//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/ForestBiome.h"

ForestBiome::ForestBiome() : Biome(Biome::FOREST, FOREST_MIN, FOREST_MAX, Biome::TREES, {
        BlockType::GRASS_BLOCK,
        BlockType::DIRT,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}
