//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_TAIGABIOME_H
#define VOXEL_ENGINE_TAIGABIOME_H

#include "../Biome.h"

class TaigaBiome : public Biome {
public:
    TaigaBiome();
};


#endif //VOXEL_ENGINE_TAIGABIOME_H
