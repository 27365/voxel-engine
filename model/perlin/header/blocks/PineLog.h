//
// Created by Michał Sobański on 15.09.2021.
//

#ifndef VOXEL_ENGINE_PINELOG_H
#define VOXEL_ENGINE_PINELOG_H

#include "../BlockType.h"

class PineLog : public BlockType {
public:
    PineLog();
};


#endif //VOXEL_ENGINE_PINELOG_H
