//
// Created by pawel on 24.08.2021.
//

#include "../header/Screen.h"

Screen::Screen(uint32_t height, uint32_t width, std::string &&title) {
    this->height = height;
    this->width = width;
    window = glfwCreateWindow(static_cast<int>(width), static_cast<int>(height), title.c_str(), nullptr, nullptr);
    if (!window) {
        glfwTerminate();
    }

    glfwMakeContextCurrent(window);
}

float Screen::getRatio() {
    return static_cast<float>(width) / static_cast<float>(height);
}

GLFWwindow *Screen::getWindow() {
    return window;
}

int Screen::getHeight() const {
    return height;
}

void Screen::setHeight(int height) {
    Screen::height = height;
}

int Screen::getWidth() const {
    return width;
}

void Screen::setWidth(int width) {
    Screen::width = width;
}
