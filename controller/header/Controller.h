//
// Created by pawel on 01.08.2021.
//

#ifndef VOXEL_ENGINE_CONTROLLER_H
#define VOXEL_ENGINE_CONTROLLER_H

#include <glm/vec3.hpp>

class Screen;

class World;

class GLFWwindow;

typedef struct Click {
    bool old;
    bool current;
} ClickData;

class Controller {
public:
    static Controller *getInstance();

    void processInput(GLFWwindow *window);

    static void framebuffer_size_callback(GLFWwindow *window, int width, int height);

    void bindScreen(Screen *screen);

    void bindWorld(World *world);

private:
    Controller();

    ~Controller();

    static World *world;

    static Screen *screen;
    static Controller *controller;
    static bool window_focused;

    static ClickData leftClick;
    static ClickData rightClick;

    static bool singlePressSwitchBlock; // false - default -> enables press, press-> set true, enables release
};


#endif //VOXEL_ENGINE_CONTROLLER_H
