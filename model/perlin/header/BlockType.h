//
// Created by Michał Sobański on 01.09.2021.
//

#ifndef VOXEL_ENGINE_BLOCKTYPE_H
#define VOXEL_ENGINE_BLOCKTYPE_H

#define PLACEABLE_ARR_LEN 13

#define EAST_BLOCK(x) ((x) + 1)
#define WEST_BLOCK(x) ((x) - 1)
#define NORTH_BLOCK(y) ((y) + 1)
#define SOUTH_BLOCK(y) ((y) - 1)

#define UP_BLOCK(z) ((z) + 1)
#define DOWN_BLOCK(z) ((z) - 1)

#define sq(x) ((x)*(x))

#define CHUNK_X_BITS 4
#define CHUNK_Y_BITS 4
#define CHUNK_Z_BITS 8

#define CHUNK_X_BITMASK(x) ((x) & 0xf)
#define CHUNK_Y_BITMASK(y) ((y) & 0xf)
#define CHUNK_Z_BITMASK(z) ((z) & 0xff)

#include <cstdint>
#include <vector>
#include <string>
#include <array>
#include <map>

extern const uint8_t PlaceableBlockTypes[PLACEABLE_ARR_LEN];

typedef struct block {
    uint8_t x: 4;
    uint8_t y: 4;
    uint8_t z;
    uint8_t up: 1;
    uint8_t down: 1;
    uint8_t north: 1;
    uint8_t west: 1;
    uint8_t south: 1;
    uint8_t east: 1;
    uint8_t undef: 2;
    uint8_t blockType;
} Block;

typedef struct TextureID {
    float x;
    float y;
} TextureID;

typedef struct BlockTexture {
    TextureID top;
    TextureID side;
    TextureID bottom;
} BlockTextures;

typedef struct BlockDef {
    BlockTextures textures;
    uint8_t alpha;
    uint8_t width;
    bool diagonal;
} BlockDef;

class BlockType {
public:
    enum Id : uint8_t {
        AIR, //0
        STONE, //1
        GRASS_BLOCK, //2
        DIRT, //3
        COBBLESTONE, //4
        BEDROCK, //5
        SAND, //6
        SANDSTONE, //7
        OAK_LOG, //8
        PLANKS, //9
        PINE_LOG, //10
        SNOW, //11
        ICE, //12
        CACTUS, //13
        GRASS, //14
        WATER, //15
        OAK_LEAVES, //16
        PINE_NEEDLES, //17
        NUM_OF_BLOCK_TYPES
    };

    Id getId() const;

    virtual bool isSolid() const;
    
    virtual bool canBeBroken() const;

    static BlockType *getBlockType(BlockType::Id id);

    static BlockType *getBlockType(uint8_t id);

    static const BlockDef *getBlockDef(uint8_t blockTypeId);

    static const BlockTextures *getTextures(uint8_t blockTypeId);

    static const float getAlphaRatio(uint8_t blockTypeId);

protected:
    static std::array<BlockType *, BlockType::NUM_OF_BLOCK_TYPES> blockTypes;
    static uint16_t texturesId;
    static BlockType &&air;
    static BlockType &&stone;
    static BlockType &&grassBlock;
    static BlockType &&grass;
    static BlockType &&dirt;
    static BlockType &&cobblestone;
    static BlockType &&bedrock;
    static BlockType &&water;
    static BlockType &&sand;
    static BlockType &&sandstone;
    static BlockType &&oakLog;
    static BlockType &&oakLeaves;
    static BlockType &&pineLog;
    static BlockType &&pineNeedles;
    static BlockType &&planks;
    static BlockType &&snow;
    static BlockType &&ice;
    static BlockType &&cactus;
    const BlockType::Id id;
    const BlockDef blockDef;

    BlockType(BlockType::Id id, BlockDef &&blockDef);
};

#endif //VOXEL_ENGINE_BLOCKTYPE_H
