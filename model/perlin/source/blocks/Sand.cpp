//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Sand.h"

Sand::Sand() : BlockType(SAND, {
        {
                {0.8, 0.4}, //top
                {0.8, 0.4}, //side
                {0.8, 0.4}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
