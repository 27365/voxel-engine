//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/OakLeaves.h"

OakLeaves::OakLeaves() : BlockType(OAK_LEAVES, {
        {
                {0.4, 0.4}, //top
                {0.4, 0.4}, //side
                {0.4, 0.4}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
