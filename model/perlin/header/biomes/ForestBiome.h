//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_FORESTBIOME_H
#define VOXEL_ENGINE_FORESTBIOME_H

#include "../Biome.h"

class ForestBiome : public Biome {
public:
    ForestBiome();
};


#endif //VOXEL_ENGINE_FORESTBIOME_H
