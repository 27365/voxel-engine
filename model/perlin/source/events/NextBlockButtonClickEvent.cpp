//
// Created by pawel on 05.12.2021.
//

#include "../../header/events/NextBlockButtonClickEvent.h"

void NextBlockButtonClickEvent::handle(Perlin *perlin) {
    perlin->incBlockTypeToPlaceIndex();
}
