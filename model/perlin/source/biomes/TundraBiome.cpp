//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/TundraBiome.h"

TundraBiome::TundraBiome() : Biome(Biome::TUNDRA, TUNDRA_MIN, TUNDRA_MAX, Biome::NO_PLANTS, {
        BlockType::SNOW,
        BlockType::DIRT,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}
