//
// Created by pawel on 11.07.2021.
//
#define STB_IMAGE_IMPLEMENTATION

#include "../../../vendor/stb_image/stb_image.h"

#include "../header/Texture.h"


#ifndef GLFW_H
#define GLFW_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#endif //GLFW_H

Texture::Texture(const std::string &path) : filePath(path), localBuffer(nullptr), width(0), height(0), bbp(0) {
    stbi_set_flip_vertically_on_load(1);
    localBuffer = stbi_load(path.c_str(), &width, &height, &bbp, 4);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glad_glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glad_glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glad_glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glad_glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, localBuffer);

    //glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    if (localBuffer)
        stbi_image_free(localBuffer);
}

int Texture::getWidth() const {
    return width;
}

void Texture::setWidth(int width) {
    this->width = width;
}

int Texture::getHeight() const {
    return height;
}

void Texture::setHeight(int height) {
    this->height = height;
}

int Texture::getBbp() const {
    return bbp;
}

void Texture::setBbp(int bbp) {
    this->bbp = bbp;
}


void Texture::bind(unsigned int slot) const {
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, texture);
}

void Texture::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture() {
    glDeleteTextures(1, &texture);
}