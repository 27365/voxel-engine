//
// Created by pawel on 06.07.2021.
//

#include "../header/Chunk.h"

#include "../header/Cube.h"

Chunk::Chunk() : faceCounter_solid(0), faceCounter_transparent(0), alreadyLoaded(false),
                 vb_solid(new VertexBuffer(nullptr, sizeof(Vertex), DEFAULT_SOLID_VB_ELEM,
                                           GL_DYNAMIC_DRAW)),
                 vb_transparent(new VertexBuffer(nullptr, sizeof(Vertex), DEFAULT_NON_SOLID_VB_ELEM,
                                                 GL_DYNAMIC_DRAW)),
                 ib(nullptr), sp(nullptr), used(false),
                 va_transparent(new VertexArray()), va_solid(new VertexArray()) {

    layout.push<float>(3);  //position
    layout.push<float>(3);  //normal
    layout.push<float>(2);  //texture_position

    va_solid->bind();
    va_solid->addBuffer(*vb_solid, layout);

    va_transparent->bind();
    va_transparent->addBuffer(*vb_transparent, layout);
}


Chunk::~Chunk() {
    delete vb_solid;
    delete vb_transparent;
    delete va_solid;
    delete va_transparent;
    vb_solid = nullptr;
    vb_transparent = nullptr;
    va_solid = nullptr;
    va_transparent = nullptr;
}

const ChunkId &Chunk::getChunkId() const {
    return chunkId;
}

void Chunk::setChunkId(const ChunkId &chunkId) {
    Chunk::chunkId = chunkId;
}

void Chunk::draw_solid() {
    if (used && ib != nullptr && sp != nullptr && va_transparent)
        Renderer::getInstance()->draw(*va_solid, *ib, faceCounter_solid * INDICES_PER_FACE, *sp);
}

void Chunk::draw_transparent() {
    if (used && ib != nullptr && sp != nullptr && va_transparent)
        Renderer::getInstance()->draw(*va_transparent, *ib, faceCounter_transparent * INDICES_PER_FACE, *sp);
}

void Chunk::setIb(IndexBuffer *ib) {
    this->ib = ib;
}

void Chunk::setSp(ShaderProgram *sp) {
    this->sp = sp;
}

bool Chunk::isUsed() {
    return used;
}

void Chunk::use() {
    used = true;
}

void Chunk::freeChunk() {
    used = false;
}

void Chunk::loadNew(PerlinChunk *perlinChunk) {
    this->perlinChunk = perlinChunk;
    load();
}

bool Chunk::reloadRequired() const {
    if (alreadyLoaded == false)
        return false;
    bool changed = perlinChunk->getChangedFlag().load();
    return changed;
}

int countFaces(Block block) {
    int faces = 0;
    if (block.up) faces++;
    if (block.down) faces++;
    if (block.north) faces++;
    if (block.east) faces++;
    if (block.west) faces++;
    if (block.south) faces++;
    if (BlockType::getBlockDef(block.blockType)->diagonal) faces += 2;
    return faces;
}

int getSolidFaces(const std::array<std::vector<block> *, 18> *array_block, int max) {
    int faces = 0;
    for (int i = 0; i < max; ++i) { // < the smallest id of block with 0<alfa<1
        std::vector<block> *blocks = array_block->at(i);
        for (Block block: *blocks) {
            faces += countFaces(block);
        }
    }
    return faces;
}

int getTransparentFaces(const std::array<std::vector<block> *, 18> *array_block, int min, int max) {
    int faces = 0;
    for (int i = min; i < max; ++i) {
        std::vector<block> *blocks = array_block->at(i);
        for (Block block: *blocks) {
            faces += countFaces(block);
        }
    }
    return faces;
}


void Chunk::load() {
    faceCounter_solid = 0;
    faceCounter_transparent = 0;

    auto array_block = perlinChunk->getBlocks();
    int solid_faces = getSolidFaces(array_block, BlockType::Id::GRASS);
    if (vb_solid->getElemCount() < solid_faces * VERTEX_PER_FACE) {

        delete vb_solid;
        vb_solid = new VertexBuffer(nullptr, sizeof(Vertex),
                                    (solid_faces + DEFAULT_BUFFER_OVERHEAD * FACES_PER_BLOCK) * VERTEX_PER_FACE,
                                    GL_DYNAMIC_DRAW);

        delete va_solid;
        va_solid = new VertexArray();
        assert(vb_solid && va_solid);

        vb_solid->bind();
        va_solid->addBuffer(*vb_solid, layout);
    }

    vb_solid->bind();
    for (int i = 0; i < BlockType::Id::GRASS; ++i) { // < the smallest id of block with 0<alfa<1
        std::vector<block> *blocks = array_block->at(i);
        for (Block block: *blocks) {
            if (faceCounter_solid * VERTEX_PER_FACE < vb_solid->getElemCount())
                faceCounter_solid += Cube::getInstance()->addBlock(chunkId, block, faceCounter_solid);
            else
                assert(false);
        }
    }


    int transparent_faces = getTransparentFaces(array_block, BlockType::Id::GRASS, BlockType::Id::NUM_OF_BLOCK_TYPES);
    if (vb_transparent->getElemCount() < transparent_faces * VERTEX_PER_FACE) {
        //std::cout << "Memory Management - Vertex Buffer For Transparent Elements" << std::endl;

        delete vb_transparent;
        vb_transparent = new VertexBuffer(nullptr, sizeof(Vertex),
                                          (transparent_faces + DEFAULT_BUFFER_OVERHEAD * FACES_PER_BLOCK) *
                                          VERTEX_PER_FACE,
                                          GL_DYNAMIC_DRAW);
        delete va_transparent;
        va_transparent = new VertexArray();
        assert(vb_transparent && va_transparent);

        vb_transparent->bind();
        va_transparent->addBuffer(*vb_transparent, layout);
    }


    vb_transparent->bind();
    for (int i = BlockType::Id::GRASS; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
        std::vector<block> *blocks = array_block->at(i);
        for (Block block: *blocks) {
            if (faceCounter_transparent * VERTEX_PER_FACE < vb_transparent->getElemCount())
                faceCounter_transparent += Cube::getInstance()->addBlock(chunkId, block, faceCounter_transparent);
            else {
                assert(false);
            }
        }
    }
    alreadyLoaded = true;
    VertexBuffer::unbind();
}

int Chunk::getUsedMemory() const {
    return vb_transparent->getUsedMeory() + vb_solid->getUsedMeory();
}



