//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_GRASSBLOCK_H
#define VOXEL_ENGINE_GRASSBLOCK_H

#include "../BlockType.h"

class GrassBlock : public BlockType {
public:
    GrassBlock();
};


#endif //VOXEL_ENGINE_GRASSBLOCK_H
