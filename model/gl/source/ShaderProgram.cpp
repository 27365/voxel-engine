//
// Created by pawel on 26.07.2021.
//

#include "../header/ShaderProgram.h"
#include "../header/Shader.h"
#include "glad/glad.h"
#include <cassert>

ShaderProgram::ShaderProgram() {
    program = glCreateProgram();
}

void ShaderProgram::attach(Shader &shader) const {
    glAttachShader(program, shader.getShader());
}

void ShaderProgram::link() const {
    glLinkProgram(program);

    int success;
    char infoLog[512];

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM:FAILED\n" <<
                  infoLog << std::endl;
    }

}

void ShaderProgram::use() const {
    glUseProgram(program);
}

void ShaderProgram::detach() {
    glUseProgram(0);
}

void ShaderProgram::deleteProgram() const {
    glDeleteProgram(program);
}

unsigned int ShaderProgram::getProgram() const {
    return program;
}

int ShaderProgram::getUniformLocation(const std::string &name) {
    return glGetUniformLocation(program, name.c_str());
}

void ShaderProgram::setUniform1i(const std::string &name, int v0) {
    int location = getUniformLocation(name);
    if (location != -1) {
        glUniform1i(location, v0);
    } else {
        std::cout << "Uniform is not found: (" << name << ")" << std::endl;
        assert(false); //This assertion can be wrong in specific cases NOTICE
    }
}

void ShaderProgram::setUniform4f(const std::string &name, float v0, float v1, float v2, float v3) {
    int location = getUniformLocation(name);
    if (location != -1) {
        glUniform4f(location, v0, v1, v2, v3);
    } else {
        std::cout << "Uniform is not found: (" << name << ")" << std::endl;
        assert(false); //This assertion can be wrong in specific cases NOTICE
    }
}

void ShaderProgram::setUniform3f(const std::string &name, float v0, float v1, float v2) {
    int location = getUniformLocation(name);
    if (location != -1) {
        glUniform3f(location, v0, v1, v2);
    } else {
        std::cout << "Uniform is not found: (" << name << ")" << std::endl;
        assert(false); //This assertion can be wrong in specific cases NOTICE
    }
}

void ShaderProgram::setUniformMatrix4fv(const std::string &name, const GLfloat *value) {
    int location = getUniformLocation(name);
    if (location != -1) {
        glUniformMatrix4fv(location, 1, GL_FALSE, value);
    } else {
        std::cout << "Uniform is not found: (" << name << ")" << std::endl;
        assert(false); //This assertion can be wrong in specific cases NOTICE
    }
}

ShaderProgram::~ShaderProgram() {

}

