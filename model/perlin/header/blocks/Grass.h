//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_GRASS_H
#define VOXEL_ENGINE_GRASS_H

#include "../BlockType.h"

class Grass : public BlockType {
public:
    Grass();

    bool isSolid() const override;
};


#endif //VOXEL_ENGINE_GRASS_H
