//
// Created by Michał Sobański on 15.09.2021.
//

#include "../../header/blocks/PineLog.h"

PineLog::PineLog() : BlockType(PINE_LOG, {
        {
                {0.8, 0.8}, //top
                {0.2, 0.2}, //side
                {0.8, 0.8}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
