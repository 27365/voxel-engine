//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_SNOW_H
#define VOXEL_ENGINE_SNOW_H

#include "../BlockType.h"

class Snow : public BlockType {
public:
    Snow();
};


#endif //VOXEL_ENGINE_SNOW_H
