//
// Created by pawel on 06.07.2021.
//

#ifndef VOXEL_ENGINE_CUBE_H
#define VOXEL_ENGINE_CUBE_H
#define _USE_MATH_DEFINES

#include <glad/glad.h>
#include <cmath>
#include "../perlin/header/PerlinChunk.h"

class VertexBuffer;

class IndexBuffer;

typedef struct Vertex {
    GLfloat position[3];
    GLfloat normal[3];
    GLfloat texturePos[2];
    //GLubyte textureID;
} Vertex;

typedef struct position {
    uint32_t x;
    uint32_t y;
    uint32_t z; //before negation
} Position;

class Cube {
private:
    Cube();


    virtual ~Cube();

    static Cube *instance;

    Vertex temp[4];

public:
    static Cube *getInstance();

    unsigned addBlock(ChunkId chunkId, Block block, unsigned accumulator);






    //Vertices

    //
    //          ********
    //          * N  2 *
    //  ************************
    //  * W   4 * D  1 * E   3 *
    //  ************************
    //          * S  5 *
    //          ********
    //          * U  0 *
    //          ********
    //
    //          ********
    //          * D/ 6 *
    //          ********
    //          ********
    //          * D\ 7 *
    //          ********
    //
    //
    constexpr const static float diagConst = (2.0f - M_SQRT2) / 4.0f;
    constexpr const static float revDiagConst = 1 - diagConst;

    constexpr const static Vertex vertices[8][4] = {
            {
                    {1.0f,             1.0f, -1.0f,             0.0f,  1.0f,  0.0f,  1.0f, 1.0f}, //UP
                    {0.0f,             1.0f, -1.0f,             0.0f,  1.0f,  0.0f,  0.0f, 1.0f},
                    {0.0f,      1.0f, 0.0f,              0.0f,  1.0f,  0.0f,  0.0f, 0.0f},
                    {1.0f,      1.0f, 0.0f,              0.0f,  1.0f,  0.0f,  1.0f, 0.0f}

            },

            {
                    {1.0f,             0.0f, -1.0f,             0.0f,  -1.0f, 0.0f,  1.0f, 0.0f}, //DOWN
                    {0.0f,             0.0f, -1.0f,             0.0f,  -1.0f, 0.0f,  0.0f, 0.0f},
                    {0.0f,      0.0f, 0.0f,              0.0f,  -1.0f, 0.0f,  0.0f, 1.0f},
                    {1.0f,      0.0f, 0.0f,              0.0f,  -1.0f, 0.0f,  1.0f, 1.0f}
            },
            {
                    {1.0f,             1.0f, -1.0f,             0.0f,  0.0f,  -1.0f, 0.0f, 1.0f}, //NORTH
                    {1.0f,             0.0f, -1.0f,             0.0f,  0.0f,  -1.0f, 0.0f, 0.0f},
                    {0.0f,      0.0f, -1.0f,             0.0f,  0.0f,  -1.0f, 1.0f, 0.0f},
                    {0.0f,      1.0f, -1.0f,             0.0f,  0.0f,  -1.0f, 1.0f, 1.0f}
            },

            {
                    {1.0f,             1.0f, 0.0f,              1.0f,  0.0f,  0.0f,  0.0f, 1.0f}, //EAST
                    {1.0f,             1.0f, -1.0f,             1.0f,  0.0f,  0.0f,  1.0f, 1.0f},
                    {1.0f,      0.0f, -1.0f,             1.0f,  0.0f,  0.0f,  1.0f, 0.0f},
                    {1.0f,      0.0f, 0.0f,              1.0f,  0.0f,  0.0f,  0.0f, 0.0f},

            },

            {
                    {0.0f,             1.0f, 0.0f,              -1.0f, 0.0f,  0.0f,  1.0f, 1.0f}, //WEST
                    {0.0f,             1.0f, -1.0f,             -1.0f, 0.0f,  0.0f,  0.0f, 1.0f},
                    {0.0f,      0.0f, -1.0f,             -1.0f, 0.0f,  0.0f,  0.0f, 0.0f},
                    {0.0f,      0.0f, 0.0f,              -1.0f, 0.0f,  0.0f,  1.0f, 0.0f},

            },
            {
                    {1.0f,             1.0f, 0.0f,              0.0f,  0.0f,  1.0f,  1.0f, 1.0f}, //SOUTH
                    {1.0f,             0.0f, 0.0f,              0.0f,  0.0f,  1.0f,  1.0f, 0.0f},
                    {0.0f,      0.0f, 0.0f,              0.0f,  0.0f,  1.0f,  0.0f, 0.0f},
                    {0.0f,      1.0f, 0.0f,              0.0f,  0.0f,  1.0f,  0.0f, 1.0f}
            },
            {
                    {revDiagConst,     1.0f, -1 * revDiagConst, 1.0f,  1.0f,  1.0f,  1.0f, 1.0f},
                    {revDiagConst,     0.0f, -1 * revDiagConst, 1.0f,  1.0f,  1.0f,  1.0f, 0.0f},
                    {diagConst, 0.0f, -1 * diagConst,    1.0f,  1.0f,  1.0f,  0.0f, 0.0f},
                    {diagConst, 1.0f, -1 * diagConst,    1.0f,  1.0f,  1.0f,  0.0f, 1.0f}
            },
            {
                    {1.0f - diagConst, 1.0f, -1 * diagConst,    1.0f,  1.0f,  1.0f,  1.0f, 1.0f},
                    {1.0f - diagConst, 0.0f, -1 * diagConst,    1.0f,  1.0f,  1.0f,  1.0f, 0.0f},
                    {diagConst, 0.0f, -1 * revDiagConst, 1.0f,  1.0f,  1.0f,  0.0f, 0.0f},
                    {diagConst, 1.0f, -1 * revDiagConst, 1.0f,  1.0f,  1.0f,  0.0f, 1.0f}
            }

    };

    constexpr const static unsigned int indices[] = {
            0, 1, 3,
            1, 2, 3
    };
};


#endif //VOXEL_ENGINE_CUBE_H
