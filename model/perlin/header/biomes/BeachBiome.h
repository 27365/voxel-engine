//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_BEACHBIOME_H
#define VOXEL_ENGINE_BEACHBIOME_H

#include "../Biome.h"

class BeachBiome : public Biome {
public:
    BeachBiome();
};


#endif //VOXEL_ENGINE_BEACHBIOME_H
