//
// Created by Michał Sobański on 15.09.2021.
//

#include "../../header/blocks/Cactus.h"

Cactus::Cactus() : BlockType(CACTUS, {
        {
                {0.4, 0.8}, //top
                {0.2, 0.8}, //side
                {0.4, 0.8}  //bottom
        },
        6, //alpha
        16, //width
        false //diagonal
}) {}
