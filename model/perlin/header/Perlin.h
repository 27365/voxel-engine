//
// Created by Michał Sobański on 06.08.2021.
//

#ifndef VOXEL_ENGINE_PERLIN_H
#define VOXEL_ENGINE_PERLIN_H

#include "PerlinChunk.h"
#include "Biome.h"

#define PERM_VECTOR_SIZE 256
#define PERLIN_CACHE_SIZE static_cast<int>(visibleChunks() * 2)

#define BIOME_NUM_OF_COEFF 4
#define SCALE_INPUT_MODIF 0.025
#define SCALE_Z_MODIF 80.25
#define BIOME_INPUT_MODIF 0.038
#define LAND_INPUT_MODIF 0.100
#define LAND_OUTPUT_MODIF 40.0
#define BASE_POWER_SUMMAND 0.75
#define LAND_POWER_SUMMAND 0.35
#define BASE_POWER_MULTIPLICAND 2.0
#define LAND_POWER_MULTIPLICAND 1.0
#define BASE_POWER_MODIF 0.25
#define LAND_POWER_MODIF 0.10
#define BIOME_INPUT_TRANSLATION 1000.0

#include <cstdint>
#include <vector>
#include <thread>
#include <list>
#include <atomic>
#include <glm/vec3.hpp>

class World;

// Improved Perlin Noise
// http://mrl.nyu.edu/~perlin/noise/
class Perlin {
public:
    explicit Perlin(World *world);

    virtual ~Perlin();

    void printFacesStats(uint32_t numOfChunks);

    void setCameraPoint(glm::vec3 point);

    void setCameraVector(glm::vec3 vector);

    const float *getCameraPoint() const;

    const float *getCameraVector() const;

    uint8_t getBlockTypeToPlace() const;

    void incBlockTypeToPlaceIndex();

    void decBlockTypeToPlaceIndex();

    PerlinChunk *generateChunk(const ChunkId &id);

    void printCameraDirection();

private:
    World *world;
    std::list<PerlinChunk *> *chunks;
    std::vector<int32_t> perm; // permutation vector
    std::atomic<bool> savingInProgress;
    std::thread thread_generator;
    std::thread thread_event_handler;
    std::thread thread_saving;
    uint8_t blockTypeToPlaceIndex;
    float cameraPoint[3];
    float cameraVector[3];

    void cacheData(PerlinChunk *newChunk);

    PerlinChunk *getCachedData(const ChunkId &id);

    uint32_t generateZ(uint32_t x, uint32_t y);

    Biome::Id generateBiome(uint32_t x, uint32_t y, uint32_t z);

    static double fade(double t);

    static double lerp(double t, double a, double b);

    static double grad(int32_t hash, double x, double y, double z);

    double noise(double x, double y, double z);

    double coeff2D(double x, double y, double typeModif);

    void addDiffFreq2D(double x, double y, double *z);

    static void rescaleZ(double *z);

    void powerNoise2D(double x, double y, double *z, double summand,
                      double multiplicand, double powerModif);

    void generateBase(double x, double y, double *z);

    void generateLandform(double x, double y, double *z);

    void generateWorld();

    void handleEvents();
};


#endif //VOXEL_ENGINE_PERLIN_H
