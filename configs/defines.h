//
// Created by pawel on 27.07.2021.
//

#ifndef VOXEL_ENGINE_DEFINES_H
#define VOXEL_ENGINE_DEFINES_H

#define DEBUG 1
#define PRINT_POSITION_INFO 0
#define PRINT_DIRECTION_INFO 1

#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3
#define FORWARD 4
#define BACKWARD 5
#define DIAG1 6
#define DIAG2 7

#define NORTH 2
#define EAST 3
#define WEST 4
#define SOUTH 5

#define CHUNK_SIZE 16
#define CHUNK_HEIGHT 256
#define MAX_BLOCKS (CHUNK_SIZE*CHUNK_SIZE*CHUNK_HEIGHT)
#define MAX_FACES (MAX_BLOCKS*6)
#define INDICES_PER_FACE 6
#define VERTEX_PER_FACE 4
#define FACES_PER_BLOCK 6
#define VERTEX_BUFFER_ELEMENTS (MAX_FACES*VERTEX_PER_FACE) // *24 -> 9 437 184 | *4 -> 1 572 864
#define INDEX_BUFFER_ELEMENTS (MAX_FACES*INDICES_PER_FACE)

#define DEFAULT_SOLID_FACES_PER_CHUNK 10
#define DEFAULT_NON_SOLID_FACES_PER_CHUNK 10
#define DEFAULT_BUFFER_OVERHEAD 100

#define DEFAULT_SOLID_VB_ELEM (DEFAULT_SOLID_FACES_PER_CHUNK * VERTEX_PER_FACE)
#define DEFAULT_NON_SOLID_VB_ELEM (DEFAULT_NON_SOLID_FACES_PER_CHUNK * VERTEX_PER_FACE)

#define VERTEX_SHADER_PATH "../shaders/vertexShader.vert"
#define FRAGMENT_SHADER_PATH "../shaders/fragmentShader.frag"
#define TEXTURE_DIR std::string("../view/res/textures/")
#define TEXTURES_IN_ROW 5
#define visibleChunks() static_cast<int>(pow(JsonConfig::getInstance()->getRenderDistance() * 2 + 1, 2))

#define GRASP_RADIUS 7.0f

#endif //VOXEL_ENGINE_DEFINES_H
