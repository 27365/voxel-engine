//
// Created by pawel on 11.07.2021.
//
#ifndef VOXEL_ENGINE_VERTEXBUFFER_H
#define VOXEL_ENGINE_VERTEXBUFFER_H


class VertexBuffer {
public:
    ///*data: start data;
    ///elemSize: size of one element e.g. sizeof(Vertex);
    ///elemCount: no of elements;
    ///mode: STATIC or DYNAMIC;
    VertexBuffer(const void *data, unsigned int elemSize, unsigned int elemCount, unsigned int mode);

    void bind() const;

    static void unbind();

    unsigned int getElemSize() const;

    unsigned int getElemCount() const;

    unsigned int getUsedMeory() const;

    ~VertexBuffer();

private:
    unsigned int rendererID;
    const unsigned int elemSize;
    unsigned int elemCount;
};


#endif //VOXEL_ENGINE_VERTEXBUFFER_H
