//
// Created by Michał Sobański on 27.09.2021.
//

#ifndef VOXEL_ENGINE_RAYPLANEINTERSECTION_H
#define VOXEL_ENGINE_RAYPLANEINTERSECTION_H

#include <cstdint>
#include <cmath>
#include "BlockType.h"

typedef uint8_t faceType;

class RayPlaneIntersection {
private:
    class Ranges;

    class Point {
    public:
        Point(Point &p);

        Point(Point &&p) noexcept;

        Point(float x, float y, float z);

        explicit Point(const float xyz[3]);

        virtual Point operator-(Point const &p);

        Point translate(faceType faceType) const;

        Point toFaceCenter(faceType faceType) const;

        Ranges toFaceRanges() const;

        float distanceBetween(Point const &p);

        float x = 0;
        float y = 0;
        float z = 0;
    };

    class Vector : public Point {
    public:
        explicit Vector(Point &&p);
    };

    class Ray {
    public:
        Ray(const float vector[3], const float point[3]);

        Ray(Vector &vec, Point &pt);

        float vec[3];
        float pt[3];
    };

    class Plane : public Ray {
    public:
        Plane(Vector &&vec, Point &pt);

        std::pair<bool, Point> getIntersectionPoint(const Ray &ray);
    };

    class Ranges {
    public:
        std::pair<float, float> xRange;
        std::pair<float, float> yRange;
        std::pair<float, float> zRange;

        bool isInRange(Point *p) const {
            return (p->x >= xRange.first && p->x <= xRange.second)
                   && (p->y >= yRange.first && p->y <= yRange.second)
                   && (p->z >= zRange.first && p->z <= zRange.second);
        }
    };

    static void blockToFaces(std::vector<faceType> *faceTypes,
                             Block *block);

public:
    static std::pair<float, faceType> nearestIntersectedFace(Block *block,
                                                             const float blockXYZ[3],
                                                             const float rayPointXYZ[3],
                                                             const float rayVectorXYZ[3]);
};


#endif //VOXEL_ENGINE_RAYPLANEINTERSECTION_H
