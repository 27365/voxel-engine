//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_SAND_H
#define VOXEL_ENGINE_SAND_H

#include "../BlockType.h"

class Sand : public BlockType {
public:
    Sand();
};


#endif //VOXEL_ENGINE_SAND_H
