//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Dirt.h"

Dirt::Dirt() : BlockType(DIRT, {
        {
                {0.0, 0.6}, //top
                {0.0, 0.6}, //side
                {0.0, 0.6}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
