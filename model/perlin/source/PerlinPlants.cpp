//
// Created by Michał Sobański on 12.09.2021.
//

#include "../header/PerlinPlants.h"

#include <cmath>

const double PerlinPlants::pi = std::acos(-1);

void PerlinPlants::generatePlants() {
    typedef struct Coords { int16_t x, y; } Coords;
    std::vector<Coords> *coords = new std::vector<Coords>[Biome::NUM_OF_PLANTS_TYPES];
    for (int16_t x = 0; x < CHUNK_SIZE; ++x) {
        for (int16_t y = 0; y < CHUNK_SIZE; ++y) {
            uint8_t biomeId = biome[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)];
            Biome::PlantsType plantsType = Biome::getPlantsType(biomeId);
            if (plantsType != Biome::NO_PLANTS) {
                coords[plantsType].push_back({x, y});
            }
        }
    }
    for (uint8_t type = 0; type < Biome::NUM_OF_PLANTS_TYPES; ++type) {
        if (type == Biome::NO_PLANTS) continue;
        if (coords[type].size() < 2) continue;
        const uint8_t numOfPlants = getNumOfPlants(type, coords[type].size());
        if (numOfPlants < 1) continue;

        for (uint8_t n = 0; n < numOfPlants; n++) {
            bool correctValues;
            Coords c;
            uint8_t attempt = 0;
            do {
                correctValues = false;
                if (++attempt > MAX_ATTEMPTS) break;
                uint64_t minCoord, maxCoord;
                if (type == Biome::TREES) {
                    radius = rng->randInt(MIN_RADIUS, MAX_RADIUS);
                    minCoord = radius;
                    maxCoord = CHUNK_SIZE - 1 - radius;
                } else {
                    minCoord = 1;
                    maxCoord = CHUNK_SIZE - 2;
                }
                c = coords[type][rng->randInt(0, coords[type].size() - 1)];
                correctValues = std::min(c.x, c.y) >= minCoord
                                && std::max(c.x, c.y) <= maxCoord;
            } while (!correctValues || !freeFields->test(PerlinChunk::getBitPosition(c.x, c.y)));

            if (attempt > MAX_ATTEMPTS || !correctValues) {
                continue;
            }

            int16_t spaceBetween = 1;
            if (type == Biome::TREES) spaceBetween = SPACE_BETWEEN_TREES;
            else if (type == Biome::GRASSES) spaceBetween = SPACE_BETWEEN_GRASSES;
            else if (type == Biome::CACTUSES) spaceBetween = SPACE_BETWEEN_CACTUSES;
            for (int16_t _x = c.x - spaceBetween; (_x < CHUNK_SIZE) && (_x <= (c.x + spaceBetween)); _x++) {
                if (_x < 0) continue;
                for (int16_t _y = c.y - spaceBetween; (_y < CHUNK_SIZE) && (_y <= (c.y + spaceBetween)); _y++) {
                    if (_y < 0) continue;
                    freeFields->reset(PerlinChunk::getBitPosition(_x, _y));
                }
            }
            if (type == Biome::TREES) addTree(c.x, c.y);
            else if (type == Biome::GRASSES) addGrass(c.x, c.y);
            else if (type == Biome::CACTUSES) addCactus(c.x, c.y);
        }
    }
    delete[] coords;
}

uint8_t PerlinPlants::getNumOfPlants(const uint8_t plantsType, const uint16_t numOfBlocks) {
    uint8_t numOfPlants = 0;
    if (plantsType == Biome::TREES) numOfPlants = rng->randInt(MIN_NUM_OF_TREES, MAX_NUM_OF_TREES);
    else if (plantsType == Biome::GRASSES) numOfPlants = rng->randInt(MIN_NUM_OF_GRASSES, MAX_NUM_OF_GRASSES);
    else if (plantsType == Biome::CACTUSES) numOfPlants = (rng->withProb(CACTUS_PROBABILITY)) & 0x1;
    const double fraction = static_cast<double>(numOfBlocks) / (CHUNK_SIZE * CHUNK_SIZE);
    return static_cast<uint8_t>(round(fraction * numOfPlants));
}

double PerlinPlants::leavesFunc(double x, double shapeCoeff, double leavesHeight) {
    return sin(pi * pow(x / leavesHeight, shapeCoeff));
}

PerlinPlants::PerlinPlants(const ChunkId *id, const uint64_t *worldSeed,
                           uint8_t **biome, uint32_t **elevation,
                           std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *blocks)
        : rng(nullptr), shapeCoeff(0), leavesStart(0), radius(0),
          trunkHeight(0), leavesHeight(0), id(id), worldSeed(worldSeed),
          biome(biome), elevation(elevation), blocks(blocks) {
    uint64_t seed = uint64_t(id->x) + 1;
    seed = seed << 32;
    seed = seed | (uint64_t(id->y) + 1);
    seed = seed + (*worldSeed);
    rng = new SimpleRng(seed);
    plantsBlocks = new std::bitset<MAX_BLOCKS>;
    leavesBlocks = new std::bitset<MAX_BLOCKS>;
    freeFields = new std::bitset<CHUNK_SIZE * CHUNK_SIZE>;
    freeFields->set();
}

PerlinPlants::~PerlinPlants() {
    delete rng;
    delete plantsBlocks;
    delete leavesBlocks;
    delete freeFields;
}

uint8_t PerlinPlants::getLeavesType(uint8_t biomeId) {
    if (biomeId == Biome::FOREST) return BlockType::OAK_LEAVES;
    if (biomeId == Biome::TAIGA) return BlockType::PINE_NEEDLES;
    return BlockType::NUM_OF_BLOCK_TYPES;
}

uint8_t PerlinPlants::getTrunkType(uint8_t biomeId) {
    if (biomeId == Biome::FOREST) return BlockType::OAK_LOG;
    if (biomeId == Biome::TAIGA) return BlockType::PINE_LOG;
    return BlockType::NUM_OF_BLOCK_TYPES;
}

void PerlinPlants::addTree(const int16_t x, const int16_t y) {
    const uint8_t biomeId = biome[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)];
    const uint8_t leavesType = getLeavesType(biomeId);
    const uint8_t trunkType = getTrunkType(biomeId);

    shapeCoeff = rng->randDouble(MIN_SHAPE_COEFF, MAX_SHAPE_COEFF);
    leavesStart = rng->randInt(MIN_LEAVES_START, MAX_LEAVES_START);
    trunkHeight = rng->randInt(MIN_TRUNK_HEIGHT, MAX_TRUNK_HEIGHT);
    if ((trunkHeight - leavesStart) < MIN_LEAVES_HEIGHT) {
        trunkHeight += (MIN_LEAVES_HEIGHT - (trunkHeight - leavesStart));
    }
    leavesHeight = trunkHeight - leavesStart + 1;

    for (int64_t h = trunkHeight + 1, lH = leavesHeight; h >= 1; h--) {
        const double r = ceil(radius * leavesFunc(lH--, shapeCoeff, leavesHeight));
        for (int64_t _x = x - radius; _x <= (x + radius); _x++) {
            for (int64_t _y = y - radius; _y <= (y + radius); _y++) {
                Block bl;
                bl.x = CHUNK_X_BITMASK(_x);
                bl.y = CHUNK_Y_BITMASK(_y);
                bl.z = CHUNK_Z_BITMASK(h + elevation[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)]);
                if (elevation[CHUNK_ARR_REAL_X(_x)][CHUNK_ARR_REAL_Y(_y)] >= bl.z) {
                    continue;
                }
                const uint32_t bitPos = PerlinChunk::getBitPosition(bl.x, bl.y, bl.z);
                if (_x == x && _y == y && h <= trunkHeight) {
                    /* trunk */
                    if (plantsBlocks->test(bitPos)) {
                        if (!leavesBlocks->test(bitPos)) {
                            continue;
                        } else {
                            for (auto it = blocks->begin(); it != blocks->end(); ++it) {
                                std::vector<Block> *vector = *it;
                                for (auto it2 = vector->begin(); it2 != vector->end(); ++it2) {
                                    Block block = *it2;
                                    if (block.x == bl.x && block.y == bl.y && block.z == bl.z) {
                                        vector->erase(it2);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    bl.up = (h == trunkHeight) & 0x1;
                    bl.down = 0x0;
                    bl.north = (elevation[CHUNK_ARR_REAL_X(bl.x)][NORTH_BLOCK(CHUNK_ARR_REAL_Y(bl.y))]
                                < bl.z) & 0x1;
                    bl.west = (elevation[WEST_BLOCK(CHUNK_ARR_REAL_X(bl.x))][CHUNK_ARR_REAL_Y(bl.y)]
                               < bl.z) & 0x1;
                    bl.south = (elevation[CHUNK_ARR_REAL_X(bl.x)][SOUTH_BLOCK(CHUNK_ARR_REAL_Y(bl.y))]
                                < bl.z) & 0x1;
                    bl.east = (elevation[EAST_BLOCK(CHUNK_ARR_REAL_X(bl.x))][CHUNK_ARR_REAL_Y(bl.y)]
                               < bl.z) & 0x1;
                    bl.blockType = trunkType & 0xff;
                    blocks->at(bl.blockType)->push_back(bl);
                    plantsBlocks->set(bitPos);
                } else if (lH >= 1 && sq(_x - x) + sq(_y - y) <= sq(r)) {
                    /* leaves */
                    if (plantsBlocks->test(bitPos)) {
                        continue;
                    }
                    bl.up = 0x1;
                    bl.down = 0x1;
                    bl.north = 0x1;
                    bl.west = 0x1;
                    bl.south = 0x1;
                    bl.east = 0x1;
                    bl.blockType = leavesType & 0xff;
                    blocks->at(bl.blockType)->push_back(bl);
                    plantsBlocks->set(bitPos);
                    leavesBlocks->set(bitPos);
                }
            }
        }
    }
}

void PerlinPlants::addCactus(const int16_t x, const int16_t y) {
    for (uint8_t h = 1; h <= CACTUS_HEIGHT; ++h) {
        Block block;
        block.x = CHUNK_X_BITMASK(x);
        block.y = CHUNK_Y_BITMASK(y);
        block.z = CHUNK_Z_BITMASK(h + elevation[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)]);
        block.up = (h == CACTUS_HEIGHT) & 0x1;
        block.down = 0x0;
        block.north = 0x1;
        block.west = 0x1;
        block.south = 0x1;
        block.east = 0x1;
        block.blockType = (BlockType::CACTUS) & 0xff;
        blocks->at(block.blockType)->push_back(block);
    }
}

void PerlinPlants::addGrass(const int16_t x, const int16_t y) {
    Block block;
    block.x = CHUNK_X_BITMASK(x);
    block.y = CHUNK_Y_BITMASK(y);
    block.z = CHUNK_Z_BITMASK(1 + elevation[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)]);
    block.up = 0x0;
    block.down = 0x0;
    block.north = 0x0;
    block.west = 0x0;
    block.south = 0x0;
    block.east = 0x0;
    block.blockType = (BlockType::GRASS) & 0xff;
    blocks->at(block.blockType)->push_back(block);
}
