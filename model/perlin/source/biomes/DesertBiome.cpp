//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/DesertBiome.h"

DesertBiome::DesertBiome() : Biome(Biome::DESERT, DESERT_MIN, DESERT_MAX, Biome::CACTUSES, {
        BlockType::SAND,
        BlockType::SAND,
        BlockType::SANDSTONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}
