//
// Created by Michał Sobański on 01.09.2021.
//

#include "../header/BlockType.h"
#include "../header/blocks/Air.h"
#include "../header/blocks/Stone.h"
#include "../header/blocks/GrassBlock.h"
#include "../header/blocks/Grass.h"
#include "../header/blocks/Dirt.h"
#include "../header/blocks/Cobblestone.h"
#include "../header/blocks/Bedrock.h"
#include "../header/blocks/Water.h"
#include "../header/blocks/Sand.h"
#include "../header/blocks/Sandstone.h"
#include "../header/blocks/OakLog.h"
#include "../header/blocks/OakLeaves.h"
#include "../header/blocks/PineLog.h"
#include "../header/blocks/PineNeedles.h"
#include "../header/blocks/Planks.h"
#include "../header/blocks/Snow.h"
#include "../header/blocks/Ice.h"
#include "../header/blocks/Cactus.h"

uint16_t BlockType::texturesId = 1;

std::array<BlockType *, BlockType::NUM_OF_BLOCK_TYPES> BlockType::blockTypes = {nullptr};

BlockType &&BlockType::air = Air();
BlockType &&BlockType::stone = Stone();
BlockType &&BlockType::grassBlock = GrassBlock();
BlockType &&BlockType::grass = Grass();
BlockType &&BlockType::dirt = Dirt();
BlockType &&BlockType::cobblestone = Cobblestone();
BlockType &&BlockType::bedrock = Bedrock();
BlockType &&BlockType::water = Water();
BlockType &&BlockType::sand = Sand();
BlockType &&BlockType::sandstone = Sandstone();
BlockType &&BlockType::oakLog = OakLog();
BlockType &&BlockType::oakLeaves = OakLeaves();
BlockType &&BlockType::pineLog = PineLog();
BlockType &&BlockType::pineNeedles = PineNeedles();
BlockType &&BlockType::planks = Planks();
BlockType &&BlockType::snow = Snow();
BlockType &&BlockType::ice = Ice();
BlockType &&BlockType::cactus = Cactus();

BlockType::BlockType(BlockType::Id id, BlockDef &&blockDef)
        : id(id), blockDef(blockDef) {
    blockTypes[this->id] = this;
}

BlockType *BlockType::getBlockType(uint8_t id) {
    return getBlockType(static_cast<BlockType::Id>(id));
}

BlockType *BlockType::getBlockType(BlockType::Id id) {
    return blockTypes[id];
}

BlockType::Id BlockType::getId() const {
    return id;
}

bool BlockType::isSolid() const {
    return true;
}

bool BlockType::canBeBroken() const {
    return true;
}

const BlockDef *BlockType::getBlockDef(uint8_t blockTypeId) {
    return &getBlockType(blockTypeId)->blockDef;
}

const BlockTextures *BlockType::getTextures(uint8_t blockTypeId) {
    return &getBlockDef(blockTypeId)->textures;
}

const float BlockType::getAlphaRatio(uint8_t blockTypeId) {
    const BlockDef *def = getBlockDef(blockTypeId);
    return static_cast<float>(def->alpha) / static_cast<float>(def->width);
}

const uint8_t PlaceableBlockTypes[PLACEABLE_ARR_LEN] = {
        BlockType::Id::DIRT,
        BlockType::Id::STONE,
        BlockType::Id::GRASS_BLOCK,
        BlockType::Id::COBBLESTONE,
        BlockType::Id::SAND,
        BlockType::Id::SANDSTONE,
        BlockType::Id::OAK_LOG,
        BlockType::Id::PLANKS,
        BlockType::Id::PINE_LOG,
        BlockType::Id::SNOW,
        BlockType::Id::ICE,
        BlockType::Id::OAK_LEAVES,
        BlockType::Id::PINE_NEEDLES,
};
