//
// Created by Michał Sobański on 15.09.2021.
//

#ifndef VOXEL_ENGINE_CACTUS_H
#define VOXEL_ENGINE_CACTUS_H

#include "../BlockType.h"

class Cactus : public BlockType {
public:
    Cactus();
};


#endif //VOXEL_ENGINE_CACTUS_H
