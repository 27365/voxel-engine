//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Stone.h"

Stone::Stone() : BlockType(STONE, {
        {
                {0.8, 0.6}, //top
                {0.8, 0.6}, //side
                {0.8, 0.6}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
