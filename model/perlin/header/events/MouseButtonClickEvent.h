//
// Created by ms on 03.10.2021.
//

#ifndef VOXEL_ENGINE_MOUSEBUTTONCLICKEVENT_H
#define VOXEL_ENGINE_MOUSEBUTTONCLICKEVENT_H

#include "../../header/RayPlaneIntersection.h"
#include "PerlinEvent.h"

typedef std::tuple<Block *, PerlinChunk *, uint32_t, uint32_t, uint32_t> BlockTuple;

class MouseButtonClickEvent : public PerlinEvent {
public:
    void handle(Perlin *perlin) override;

protected:
    typedef struct NearestBlocks {
        BlockTuple centralBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
        BlockTuple upBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
        BlockTuple downBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
        BlockTuple northBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
        BlockTuple westBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
        BlockTuple southBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
        BlockTuple eastBlock = std::tuple(nullptr, nullptr, 0, 0, 0);
    } NearestBlocks;
    typedef struct FaceInfo {
        float distance;
        faceType fType;
        uint32_t x;
        uint32_t y;
        uint32_t z;
    } FaceInfo;

    virtual void modifyBlocks(Perlin *perlin, const NearestBlocks &nearestBlocks) = 0;

    virtual bool isPlaceBlockEvent() = 0;

    static Block getNewBlock(const BlockTuple *t);

    static uint8_t getBlockType(const BlockTuple *t);

private:
    static Block *getBlock(PerlinChunk *chunk, uint32_t x, uint32_t y, uint32_t z);

    static BlockTuple getBlockPair(Perlin *perlin, uint32_t x, uint32_t y, uint32_t z);

    static PerlinChunk *getChunk(Perlin *perlin, uint32_t x, uint32_t y);
};


#endif //VOXEL_ENGINE_MOUSEBUTTONCLICKEVENT_H
