//
// Created by pawel on 11.07.2021.
//

#include "../header/IndexBuffer.h"
#include "../header/Renderer.h"

IndexBuffer::IndexBuffer(const unsigned int *data, unsigned int count) : count(count) {
    glGenBuffers(1, &rendererID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(GLuint), data, GL_STATIC_DRAW);
}

void IndexBuffer::bind() const {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererID);
}

void IndexBuffer::unbind() {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

IndexBuffer::~IndexBuffer() {
    glDeleteBuffers(1, &rendererID);
}

unsigned int IndexBuffer::getCount() const {
    return count;
}
