//
// Created by Michał Sobański on 25.08.2021.
//

#ifndef VOXEL_ENGINE_JSONCONFIG_H
#define VOXEL_ENGINE_JSONCONFIG_H

#define JSON_INDENTATION 2
#define CONFIG_FILE_PATH "../configs/config.json"
#define RENDER_DIST_JSON_KEY "Render_distance"
#define SENSITIVITY_JSON_KEY "Sensitivity"
#define SPEED_JSON_KEY "Speed"
#define FOV_JSON_KEY "FOV"
#define TITLE_JSON_KEY "Title"
#define HEIGHT_JSON_KEY "Height"
#define WIDTH_JSON_KEY "Width"

#include "../json/json.hpp"

#include <iomanip>
#include <mutex>
#include <iostream>
#include <fstream>
#include <string>

/*
 * [std::string]  title
 * [uint32_t]     height
 * [uint32_t]     width
 * [uint8_t]      render distance
 * [float]        FOV
 * [float]        sensitivity
 * [float]        speed
 */
class JsonConfig {
public:
    static JsonConfig *getInstance();

    void print();

    uint8_t getRenderDistance();

    void setRenderDistance(uint8_t renderDistance);

    float getSensitivity();

    void setSensitivity(float sensitivity);

    float getSpeed();

    void setSpeed(float speed);

    float getFov();

    void setFov(float fov);

    std::string getTitle();

    void setTitle(std::string &title);

    uint32_t getHeight();

    void setHeight(uint32_t height);

    uint32_t getWidth();

    void setWidth(uint32_t width);

private:
    static JsonConfig *jsonConfig;
    std::mutex ioMutex;
    std::mutex objMutex;
    nlohmann::basic_json<std::map, std::vector, std::string, bool, int64_t,
            uint64_t, double, std::allocator, nlohmann::adl_serializer,
            std::vector<std::uint8_t>> json;

    JsonConfig();

    void readConfigFile();

    void writeToConfigFile();

    template<class Type>
    Type getValue(const std::string &jsonKey);

    template<class Type>
    void setValue(const std::string &jsonKey, const Type &value);
};


#endif //VOXEL_ENGINE_JSONCONFIG_H
