//
// Created by pawel on 11.07.2021.
//

#include "../header/Shader.h"

Shader::Shader(const char *filePath, unsigned int type) {
    loadFromFile(filePath);
    compileShader(type);
}

void Shader::loadFromFile(const char *filepath) {
    std::ifstream shaderFile;
    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        // open files
        shaderFile.open(filepath);
        std::stringstream shaderStream;
        // read file’s buffer contents into streams
        shaderStream << shaderFile.rdbuf();
        // close file handlers
        shaderFile.close();
        // convert stream into string
        code = shaderStream.str();
    }
    catch (std::ifstream::failure &e) {
        std::cout << "ERROR::SHADER::READ_FROM_FILE" << std::endl;
    }
}

void Shader::compileShader(unsigned int type) {
    shader = glCreateShader(type);
    const char *shaderCode = code.c_str();
    glShaderSource(shader, 1, &shaderCode, nullptr);
    glCompileShader(shader);

    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (!success) {
        glGetShaderInfoLog(shader, LOG_MAX_LENGTH, nullptr, logInfo);
        std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << logInfo << std::endl;
    }

}

unsigned int Shader::getShader() const {
    return shader;
}

void Shader::deleteShader() const {
    glDeleteShader(shader);
}

Shader::~Shader() {

}
