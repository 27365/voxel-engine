//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_DESERTBIOME_H
#define VOXEL_ENGINE_DESERTBIOME_H

#include "../Biome.h"

class DesertBiome : public Biome {
public:
    DesertBiome();
};


#endif //VOXEL_ENGINE_DESERTBIOME_H
