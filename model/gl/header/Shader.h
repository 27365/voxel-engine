//
// Created by pawel on 11.07.2021.
//

#ifndef VOXEL_ENGINE_SHADER_H
#define VOXEL_ENGINE_SHADER_H

#include <glad/glad.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#define LOG_MAX_LENGTH 512

class Shader {
public:

    Shader(const char *filePath, unsigned int type);

    unsigned int getShader() const;


    void deleteShader() const;

    //void use();

    //void setBool(const std::string &name, bool value) const;

    //void setInt(const std::string &name, int value) const;

    //void setFloat(const std::string &name, float value) const;

    ~Shader();

private:
    unsigned int shader;

    void loadFromFile(const char *name);

    void compileShader(unsigned int type);

    char logInfo[LOG_MAX_LENGTH];

    std::string code;
};


#endif //VOXEL_ENGINE_SHADER_H
